fontcustom test

# Installation

まず <https://github.com/FontCustom/fontcustom#installation>

```sh
$ bundle install
```

フォントをつくる:

```sh
$ make font
```

フォントを掃除:

```sh
$ make clean
```
