.PHONY: font clean

font:
	bundle exec fontcustom compile \
	--autowidth \
	--force \
	--templates scss \
	-o css icons

clean:
	rm -f css/fontcustom* css/_fontcustom.scss
